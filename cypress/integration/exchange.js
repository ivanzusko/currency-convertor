/* global cy, Cypress */
import { inputValue } from '../utils/index'
const MOCK_RESPONSE = require('../fixtures/mock_data.json')
const API_URL = `${Cypress.env('API_URL')}${Cypress.env('API_KEY')}`

describe('Exchange page', () => {
  describe('"/exchange"', () => {
    beforeEach(() => {
      cy.server()
      cy.route({
        url: API_URL,
        response: MOCK_RESPONSE
      })
      cy.visit('http://localhost:3000/exchange')
    })

    describe('Everything rendered', () => {
      it('Renders currencies', () => {
        cy.get('[data-cy=main-pocket-currency]').contains('EUR')
        cy.get('[data-cy=exchange-to-currency]').contains('GBP')
      })

      it('Renders balance statuses', () => {
        cy.get('[data-cy=pocket-from]').find('[data-cy=balance-status]').contains('You have €100.13')
        cy.get('[data-cy=pocket-to]').find('[data-cy=balance-status]').contains('You have £777')
      })

      it('Renders current exchange rate', () => {
        cy.get('[data-cy=current-exchange-rate]').contains('€1 = £')
      })
    })

    describe('Currency exchange process', () => {
      describe('Changing the pockets', () => {
        it('Changes the currencies to be exchanged', () => {
          cy.get('[data-cy=main-pocket-currency]').contains('EUR')
          cy.get('[data-cy=exchange-to-currency]').contains('GBP')

          cy.get('[data-cy=mainCurrency-USD]').click({ force: true })
          cy.get('[data-cy=currencyToExchange-EUR]').click({ force: true })

          cy.get('[data-cy=main-pocket-currency]').contains('USD')
          cy.get('[data-cy=exchange-to-currency]').contains('EUR')
        })
      })

      describe('Cases when exchange is not possible', () => {
        it('Doesn\'t exchange when user tries to exchange from more then he/she has', () => {
          inputValue('input[name=from]').change('1000')
          cy.get('[data-cy=convertor-form]').submit()
          cy.get('[data-cy=helper-message]').contains('Not enough money on your balance')
        })

        it('Doesn\'t exchange when user tries to exchange to more then he/she has', () => {
          inputValue('input[name=to]').change('777')
          cy.get('[data-cy=convertor-form]').submit()
          cy.get('[data-cy=helper-message]').contains('Not enough money on your balance')
        })
      })

      describe('When exchanging amount was changed from being negative to positive', () => {
        it('Doesn\'t exchanges the currency when negative balance and exchanges if balance become positive again', () => {
          inputValue('input[name=from]').change('1000')
          cy.get('[data-cy=convertor-form]').submit()
          cy.get('[data-cy=helper-message]').contains('Not enough money on your balance')
          cy.get('[data-cy=pocket-from]').find('[data-cy=balance-status]').contains('You have €100.13')

          inputValue('input[name=from]').change('10')
          cy.get('[data-cy=convertor-form]').submit()
          cy.get('[data-cy=pocket-from]').find('[data-cy=balance-status]').contains('You have €90.13')
        })
      })

      describe('The exchange when user uses both inputs', () => {
        it('Exchanges from the main currency when `from` value is changing', () => {
          inputValue('input[name=from]').change('50')
          cy.get('[data-cy=convertor-form]').submit()
          cy.get('[data-cy=pocket-from]').find('[data-cy=balance-status]').contains('You have €50.13')
        })

        it('Exchanges from the main currency when `to` value is changing', () => {
          inputValue('input[name=to]').change('33.02')
          cy.get('[data-cy=convertor-form]').submit()
          cy.get('[data-cy=pocket-from]').find('[data-cy=balance-status]').contains('You have €63.97')
        })
      })
    })

    describe('Returns to the pocket', () => {
      it('Redirects back to \'/\'', () => {
        cy.get('[data-cy=to-pocket]').click({ force: true })

        cy.location().should((loc) => {
          expect(loc.pathname).to.eq('/')
        })
      })
    })
  })
})
