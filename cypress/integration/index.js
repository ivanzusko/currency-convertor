/* global cy */
describe('Home page', () => {
  describe('/', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000')
    })

    describe('Everything rendered', () => {
      it('Renders balance', () => {
        cy.get('[data-cy=balance]').contains('€ 100.13')
      })

      it('Renders pocket name', () => {
        cy.get('[data-cy=pocket-name]').contains('EUR - Euro')
      })

      it('Renders exchange button', () => {
        cy.get('[data-cy=to-exchange]').contains('Exchange')
      })
    })

    describe('Functionality', () => {
      it('Changes the pocket', () => {
        cy.get('[data-cy=pockets-USD]').click({ force: true })
        cy.get('[data-cy=pocket-name]').contains('USD - American Dollar')
      })

      it("should redirect to Exchange page('/exchange') after click on 'Exchange' button", () => {
        cy.get('[data-cy=to-exchange]').eq(0).click({ force: true })

        cy.location().should((loc) => {
          expect(loc.pathname).to.eq('/exchange')
        })
      })

      it('should change pocket and redirects to Exchange page to exchange currency of the selected pocket', () => {
        cy.get('[data-cy=pockets-USD]').click({ force: true })
        cy.get('[data-cy=to-exchange]').eq(0).click({ force: true })
        cy.get(['[data-cy=main-pocket-currency]'])
          .should($el => $el)
          .then($el => {
            cy.contains($el.selector[0], 'USD')
          })
      })
    })
  })
})
