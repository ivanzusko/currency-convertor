import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { act, cleanup, render, screen, waitForElementToBeRemoved } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import Exchange from './index'
import { PocketsProvider } from '../../context/pockets-context'
import { CurrencyProvider } from '../../context/currency-context'
import { RatesProvider } from '../../context/rates-context'

const mockData = {
  base: 'USD',
  rates: {
    USD: 1,
    EUR: 0.845798,
    GBP: 0.772224
  }
}

beforeEach(() => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(mockData)
    })
  )
})

afterEach(cleanup)

const setup = () => (
  <BrowserRouter>
    <PocketsProvider>
      <CurrencyProvider>
        <RatesProvider>
          <Exchange />
        </RatesProvider>
      </CurrencyProvider>
    </PocketsProvider>
  </BrowserRouter>
)

const getInputFrom = () => screen.getByTestId('exchange-from-input')
const getInputTo = () => screen.getByTestId('exchange-to-input')
const getHelperMessage = () => screen.queryByTestId('helper-message')
const getExchangeButton = () => screen.getByText('Exchange')
const getExchangeTo = currency => screen.getByTestId(`currencyToExchange-${currency}`)
const getExchangeFrom = currency => screen.getByTestId(`mainCurrency-${currency}`)

describe('Exchange route', () => {
  it('should render without crashing and show exchange rates', async () => {
    await act(async () => {
      render(setup())
      await waitForElementToBeRemoved(() => screen.getAllByText('€1 = £NaN'))

      screen.getByText('€1 = £0.913')
      screen.getByText('You have €100.13')
      screen.getByText('£1 = €1.1')

      expect(getHelperMessage()).toBeNull()
    })
  })

  describe('Exchanging the currencies', () => {
    it('should handle currency exchange when user fill `from` field', async () => {
      await act(async () => {
        render(setup())
        await waitForElementToBeRemoved(() => screen.getAllByText('€1 = £NaN'))

        // not enough money case
        userEvent.type(getInputFrom(), '-300')
        expect(getInputFrom().value).toBe('-300')
        userEvent.type(getInputFrom(), '200')
        expect(getInputFrom().value).toBe('-200')
        expect(getHelperMessage().textContent).toBe('Not enough money on your balance. ')

        // exchange currency by setting up the `from` amount
        userEvent.type(getInputFrom(), '20')
        expect(getInputFrom().value).toBe('-20')
        expect(getInputTo().value).toBe('+18.26')
        userEvent.click(getExchangeButton())

        expect(getHelperMessage()).toBeNull()
        screen.getByText('You have €80.13')
        screen.getByText('You have £795.26')
      })
    })

    it('should handle currency exchange when user fill `to` field', async () => {
      await act(async () => {
        render(setup())
        await waitForElementToBeRemoved(() => screen.getAllByText('€1 = £NaN'))

        // not enough money case
        userEvent.type(getInputTo(), '2000')
        expect(getInputFrom().value).toBe('-2190.55')
        expect(getInputTo().value).toBe('+2000')

        expect(getHelperMessage().textContent).toBe('Not enough money on your balance. ')
        screen.getByText('You have €100.13')

        // exchange currency by setting up the `to` amount
        userEvent.type(getInputTo(), '+10')
        expect(getInputTo().value).toBe('+10')
        userEvent.type(getInputTo(), '20')
        expect(getInputTo().value).toBe('+20')
        expect(getInputFrom().value).toBe('-21.91')
        userEvent.click(getExchangeButton())

        expect(getHelperMessage()).toBeNull()
        screen.getByText('You have €78.22')
        screen.getByText('You have £797')
      })
    })
  })

  describe('Changes of the pockets (from/to)', () => {
    it('should handle changes of the pockets', async () => {
      await act(async () => {
        render(setup())
        await waitForElementToBeRemoved(() => screen.getAllByText('€1 = £NaN'))

        // changes currency exchange to
        userEvent.click(getExchangeTo('USD'))
        screen.getByText('You have €100.13')
        screen.getByText('You have $13')

        // changes currency exchange from
        userEvent.click(getExchangeFrom('GBP'))
        screen.getByText('You have £777')
        screen.getByText('You have $13')

        // depending on the amount in the pocket show/hide helper message
        userEvent.click(getExchangeFrom('USD'))
        userEvent.click(getExchangeTo('GBP'))
        userEvent.type(getInputFrom(), '50')
        expect(getHelperMessage()).not.toBeNull()
        userEvent.click(getExchangeFrom('EUR'))
        expect(getHelperMessage()).toBeNull()
      })
    })
  })

  describe('When the exchange is not allowed', () => {
    it('should not allow to convert if the balance is negative', async () => {
      await act(async () => {
        render(setup())
        await waitForElementToBeRemoved(() => screen.getAllByText('€1 = £NaN'))

        // initial state of the pockets
        screen.getByText('You have €100.13')
        screen.getByText('You have £777')
        // enter value
        userEvent.type(getInputFrom(), '200')
        // try to convert
        userEvent.click(getExchangeButton())
        // nothing changed
        screen.getByText('You have €100.13')
        screen.getByText('You have £777')
      })
    })

    it('should not allow to convert one currency to the same currency', async () => {
      await act(async () => {
        render(setup())
        await waitForElementToBeRemoved(() => screen.getAllByText('€1 = £NaN'))

        userEvent.click(getExchangeFrom('EUR'))
        screen.getByText('You have €100.13')
        // select the same currency as target currency
        userEvent.click(getExchangeTo('EUR'))
        // enter value
        userEvent.type(getInputFrom(), '10')
        // try to convert
        userEvent.click(getExchangeButton())
        // make sure nothing changed
        // selector *All* because there should be 2 equal lables
        screen.getAllByText('You have €100.13')
      })
    })
  })
})
