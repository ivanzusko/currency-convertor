import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useCurrency } from '../../context/currency-context'
import { useRates } from '../../context/rates-context'
import { usePockets, useMainPocket } from '../../context/pockets-context'
import toDecimal from '../../utils/to-decimal'
import dynamicToDecimal from '../../utils/dynamic-to-decimal'
import formatValue from '../../utils/format-value'
import renderOptions from '../../utils/render-options'
import RadioGroup from '../../components/RadioGroup'
import Flex from '../../components/Layout/Flex'
import Space from '../../components/Layout/Space'
import Button from '../../components/Button'
import BalanceStatus from '../../components/BalanceStatus'

import './styles.css'

const inputsSetup = {
  FROM: 'from',
  TO: 'to'
}

function Exchange () {
  const {
    pockets, setPockets
  } = usePockets()
  const {
    mainPocket, setMainPocket
  } = useMainPocket()
  const {
    currencyToExchange, setCurrencyToExchange
  } = useCurrency()
  const { rate } = useRates()

  const [amountFrom, setAmountFrom] = useState('')
  const [amountTo, setAmountTo] = useState('')
  const [nonSufficientFunds, setNonSufficientFunds] = useState('')
  const [active, setActive] = useState(inputsSetup.FROM)

  React.useEffect(() => {
    const newMainAmount = pockets[mainPocket].amount - amountFrom * 1

    if (newMainAmount < 0) {
      setNonSufficientFunds('Not enough money on your balance.')
    } else {
      setNonSufficientFunds('')
    }
  }, [pockets, mainPocket, amountFrom])

  React.useEffect(() => {
    if (active === inputsSetup.FROM) {
      const value = amountFrom ? (amountFrom * rate) : ''
      setAmountTo(dynamicToDecimal(value, 2))
    }

    if (active === inputsSetup.TO) {
      const value = amountTo ? (amountTo / rate) : ''
      setAmountFrom(dynamicToDecimal(value, 2))
    }
  }, [active, amountFrom, amountTo, rate])

  const { FROM, TO } = inputsSetup

  const handleInputChange = e => {
    const { name, value } = e.target

    if (name === FROM) {
      const actualValue = value.startsWith('-') ? value.slice(1) : value
      const formatedValue = formatValue(amountFrom, actualValue)

      setAmountFrom(formatedValue)
    }

    if (name === TO) {
      const actualValue = value.startsWith('+') ? value.slice(1) : value
      const formatedValue = formatValue(amountTo, actualValue)

      setAmountTo(formatedValue)
    }

    setActive(name)
  }

  const handleSelectCurrencyFrom = e => {
    const { value } = e.target

    setMainPocket(value)
    setAmountFrom('')
  }

  const handleSelectCurrencyTo = e => {
    const { value } = e.target

    setCurrencyToExchange(value)
  }

  const handleExchange = (e) => {
    e.preventDefault()

    // do nothing
    // - if user tries to exchange from to the same currency or
    // - if not enough money
    if (mainPocket === currencyToExchange || nonSufficientFunds) {
      return
    }

    // amount from is string with negative integer, so we need to sum
    const newMainAmount = pockets[mainPocket].amount - amountFrom * 1
    const newTargetAmount = pockets[currencyToExchange].amount + amountTo * 1

    const updatedPockets = {
      ...pockets,
      // change pocket with mainCurrency
      [mainPocket]: {
        ...pockets[mainPocket],
        amount: newMainAmount
      },
      // change pocket with currencyToExchange
      [currencyToExchange]: {
        ...pockets[currencyToExchange],
        amount: newTargetAmount
      }
    }

    setPockets(updatedPockets)
    setAmountFrom('')
    setAmountTo('')
  }

  const options = renderOptions(pockets)

  return (
    <form
      method='post'
      onSubmit={handleExchange}
      className='Exchange-page page'
      data-cy='convertor-form'
    >
      <Flex direction='column' className='Exchange-page__wrapper'>
        <Flex
          className='Exchange-page__header'
          flex='1'
          align='center'
          justify='between'
        >
          <Space left size='s'>
            <Link to='/' exact='true' data-cy='to-pocket'>
              <Button>Cancel</Button>
            </Link>
          </Space>
          <div data-cy='current-exchange-rate' data-testid='current-exchange-rate'>{pockets[mainPocket].label}1 = {pockets[currencyToExchange].label}{toDecimal(rate, 4)}</div>
          <Space right size='s'>
            <Button type='submit' disabled={!amountFrom || nonSufficientFunds} data-cy='convert' data-testid='exchange-currencies'>Exchange</Button>
          </Space>
        </Flex>

        <Flex
          direction='column'
          flex='4'
          className='Exchange-page__exchange-from'
          data-cy='pocket-from'
        >
          <Flex flex='3' align='center'>
            <Space left right size='xxl' fullwidth>
              <Flex justify='between'>
                <label
                  htmlFor={FROM}
                  className='Exchange-page__input-label'
                  data-cy='main-pocket-currency'
                >
                  {mainPocket}
                </label>
                <input
                  className='Exchange-page__input-input'
                  type='text'
                  id={FROM}
                  value={amountFrom ? '-' + amountFrom : ''}
                  onChange={handleInputChange}
                  name={FROM}
                  data-testid='exchange-from-input'
                />
              </Flex>

              <BalanceStatus
                amount={toDecimal(pockets[mainPocket].amount, 2)}
                label={pockets[mainPocket].label}
                nonSufficientFunds={nonSufficientFunds}
                fullwidth
              />
            </Space>
          </Flex>

          <Flex flex='1'>
            <RadioGroup
              value={mainPocket}
              name='mainCurrency'
              onChange={handleSelectCurrencyFrom}
              options={options}
            />
          </Flex>
        </Flex>

        <Flex
          flex='4'
          direction='column'
          justify='between'
          className='Exchange-page__exchange-to'
          data-cy='pocket-to'
        >
          <Flex flex='3' align='center'>
            <Space left right size='xxl' fullwidth>
              <Flex direction='column'>
                <Flex justify='between'>
                  <label
                    htmlFor={TO}
                    className='Exchange-page__input-label'
                    data-cy='exchange-to-currency'
                  >
                    {currencyToExchange}
                  </label>
                  <input
                    className='Exchange-page__input-input'
                    type='text'
                    id={TO}
                    name={TO}
                    value={amountTo ? '+' + amountTo : ''}
                    data-testid='exchange-to-input'
                    onChange={handleInputChange}
                  />
                </Flex>

                <Flex justify='between'>
                  <BalanceStatus
                    amount={toDecimal(pockets[currencyToExchange].amount, 2)}
                    label={pockets[currencyToExchange].label}
                  />
                  <span data-testid='jo'>
                    {pockets[currencyToExchange].label}1 = {pockets[mainPocket].label}{toDecimal(1 / rate, 2)}
                  </span>
                </Flex>
              </Flex>
            </Space>
          </Flex>
          <Flex flex='1'>
            <RadioGroup
              value={currencyToExchange}
              name='currencyToExchange'
              onChange={handleSelectCurrencyTo}
              options={options}
              data-testid='target-currencies'
            />
          </Flex>
        </Flex>

      </Flex>
    </form>
  )
}

export default Exchange
