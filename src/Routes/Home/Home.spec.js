/* eslint-env jest */
import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { render, screen, fireEvent } from '@testing-library/react'
import Home from './index'
import { PocketsProvider } from '../../context/pockets-context'

describe('<Home> route', () => {
  const getPocketName = () => {
    return screen.getByTestId('pocket-name').textContent
  }

  const getBalance = () => {
    return screen.getByTestId('balance').textContent
  }

  beforeEach(() => {
    render(
      <BrowserRouter>
        <PocketsProvider>
          <Home />
        </PocketsProvider>
      </BrowserRouter>
    )
  })

  it('renders without crashing', () => {
    const ExchangeButton = screen.getByTestId('exchange-button')

    expect(getPocketName()).toEqual('EUR - Euro')
    expect(getBalance()).toEqual('€ 100.13')
    expect(ExchangeButton).not.toBe(null)
  })

  it('handles pocket changes', () => {
    fireEvent.click(screen.getByTestId('pockets-GBP'))
    expect(getPocketName()).toBe('GBP - British Pound')
    expect(getBalance()).toBe('£ 777')

    fireEvent.click(screen.getByTestId('pockets-USD'))
    expect(getPocketName()).toBe('USD - American Dollar')
    expect(getBalance()).toBe('$ 13')

    fireEvent.click(screen.getByTestId('pockets-EUR'))
    expect(getPocketName()).toBe('EUR - Euro')
    expect(getBalance()).toBe('€ 100.13')
  })
})
