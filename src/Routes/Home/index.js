import React from 'react'
import { Link } from 'react-router-dom'
import { usePockets, useMainPocket } from '../../context/pockets-context'
import toDecimal from '../../utils/to-decimal'
import Space from '../../components/Layout/Space'
import RadioGroup from '../../components/RadioGroup'
import ExchangeIcon from '../../components/ExchangeIcon'
import Flex from '../../components/Layout/Flex'

import './styles.css'

function Home () {
  const { pockets } = usePockets()
  const { mainPocket, setMainPocket } = useMainPocket()
  const { amount, label, fullName } = pockets[mainPocket]

  const handleSelectCurrencyFrom = (e) => {
    const { value } = e.target

    setMainPocket(value)
  }

  const renderOptions = Object.keys(pockets).map(item => ({
    value: item
  }))

  return (
    <div className='Pocket-page page'>

      <Flex className='Pocket-page__pocket' align='end'>
        <div data-cy='balance' data-testid='balance'>{label} {toDecimal(amount, 2)}</div>
      </Flex>

      <Flex className='Pocket-page__names'>
        <Flex direction='column' align='center'>
          <div data-cy='pocket-name' data-testid='pocket-name'>{fullName}</div>

          <RadioGroup
            value={mainPocket}
            name='pockets'
            onChange={handleSelectCurrencyFrom}
            options={renderOptions}
            data-testid='pockets'
          />
        </Flex>
      </Flex>

      <Flex className='Pocket-page__footer'>
        <Link
          to='/exchange'
          className='Pocket-page__exchange-link'
          data-testid='exchange-button'
          data-cy='to-exchange'
        >
          <Flex direction='column' align='center'>
            <ExchangeIcon />
            <Space top size='xs'>
              Exchange
            </Space>
          </Flex>
        </Link>
      </Flex>

    </div>
  )
}

export default Home
