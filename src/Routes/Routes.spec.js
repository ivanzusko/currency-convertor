/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import { Route } from 'react-router'
import Routes from './index'
import Home from './Home'
import Exchange from './Exchange'

describe('<Routes /> component', () => {
  const wrapper = shallow(<Routes />)

  it('renders without crashing and contain all Routes', () => {
    expect(wrapper.find(Route).length).toBe(2)
  })

  it('renders without crashing and contain all Routes with correct pathes', () => {
    expect(wrapper.find(Route).get(0).props.component).toEqual(Home)
    expect(wrapper.find(Route).get(1).props.component).toEqual(Exchange)
    expect(wrapper.find(Route).get(0).props.path).toEqual('/')
    expect(wrapper.find(Route).get(1).props.path).toEqual('/exchange')
  })
})
