import React from 'react'
import { Route, Switch } from 'react-router'
import Home from './Home'
import Exchange from './Exchange'

function Routes () {
  return (
    <Switch>
      <Route path='/' exact component={Home} />
      <Route path='/exchange' component={Exchange} />
    </Switch>
  )
}

export default Routes
