/* eslint-env jest */
import React from 'react'
import { render, screen } from '@testing-library/react'
import BalanceStatus from './index'

describe('<BalanceStatus> component', () => {
  it('renders without crashing', () => {
    render(<BalanceStatus
      amount='13'
      label='$'
    />)

    screen.getByText('You have $13')
  })

  it('renders with non sufficient funds message if it was passed', () => {
    render(<BalanceStatus
      amount='13'
      label='$'
      nonSufficientFunds='You know nothing, Jon Snow.'
    />)

    expect(screen.getByTestId('helper-message').textContent).toEqual('You know nothing, Jon Snow. ')
  })

  it('renders with additional className if fullwidth property was passed', () => {
    const { container } = render(<BalanceStatus
      fullwidth
    />)

    expect(container.firstChild).toHaveClass('Balance-status--fullwidth')
  })
})
