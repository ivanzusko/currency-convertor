import React from 'react'

import './styles.css'

function BalanceStatus ({ amount, label, nonSufficientFunds, fullwidth }) {
  const getFullwidth = () => fullwidth ? ' Balance-status--fullwidth' : ''
  const combinedClassName = `Balance-status${getFullwidth()}`

  return (
    <span className={combinedClassName} data-cy='balance-status'>
      {
        nonSufficientFunds &&
          <span data-testid='helper-message' data-cy='helper-message'>{nonSufficientFunds} </span>
      }
      You have {label}{amount}
    </span>
  )
}

export default BalanceStatus
