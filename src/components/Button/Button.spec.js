/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Button from './index'

describe('<Button> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Button />)

    expect(wrapper.hasClass('Button')).toEqual(true)
    expect(wrapper.props().type).toEqual('button')
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <Button>
        Button
      </Button>
    ))
    expect(wrapper.text()).toEqual('Button')
  })

  it('renders with custom type property if passed', () => {
    const wrapper = shallow(<Button type='submit' />)
    expect(wrapper.props().type).toEqual('submit')
  })

  it('renders with additional class name if respective prop was passed', () => {
    const wrapper = shallow((
      <Button className='boo' />
    ))
    expect(wrapper.hasClass('Button')).toEqual(true)
    expect(wrapper.hasClass('boo')).toEqual(true)
  })

  it('should be disabled if respective prop was passed', () => {
    const wrapper = shallow((
      <Button className='boo' disabled />
    ))
    expect(wrapper.props().disabled).toEqual(true)
  })
})
