import React from 'react'

import './Button.css'

function Button (props) {
  const { children, className, type = 'button', disabled, ...otherProps } = props
  const getClassName = () => className ? ` ${className}` : ''
  const getDisabledClass = () => disabled ? ' Button--disabled' : ''
  const combinedClassName = `Button${getClassName()}${getDisabledClass()}`

  return (
    <button
      type={type}
      className={combinedClassName}
      disabled={disabled}
      data-testid={otherProps['data-testid']}
    >
      {children}
    </button>
  )
}

export default Button
