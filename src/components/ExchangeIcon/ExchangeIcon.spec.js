/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import ExchangeIcon from './index'

describe('<Button> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(
      <ExchangeIcon
        amount='13'
        label='$'
      />
    )

    expect(wrapper.hasClass('ExchangeIcon')).toEqual(true)
    expect(wrapper.find('div.ExchangeIcon__icon')).toHaveLength(1)
    expect(wrapper.find('svg')).toHaveLength(1)
  })
})
