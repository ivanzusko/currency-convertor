/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Flex from './index'

describe('<Flex> component', () => {
  const getWrapper = props => shallow(
    <Flex {...props} />
  )

  it('renders without crashing and with all options', () => {
    const wrapper = shallow(<Flex />)

    expect(wrapper.hasClass('Flex')).toEqual(true)
  })

  it('renders children when passed in', () => {
    let wrapper = shallow(
      <Flex>
        <div className='bastard'>
          Jon Snow
        </div>
      </Flex>
    )
    expect(wrapper.find('div.bastard')).toHaveLength(1)

    wrapper = shallow(
      <Flex>
        <div className='starks'>
          <div className='stark'>
            Rob
          </div>
          <div className='stark'>
            Aria
          </div>
          <div className='stark'>
            Sansa
          </div>
        </div>
      </Flex>
    )

    expect(wrapper.find('div.starks')).toHaveLength(1)
    expect(wrapper.find('div.stark')).toHaveLength(3)
  })

  it('renders with classNames reflecting the passed props', () => {
    let wrapper = getWrapper({ flex: 2, justify: 'center', align: 'end' })
    expect(wrapper.hasClass('Flex--flex-2')).toEqual(true)
    expect(wrapper.hasClass('Flex--align-end')).toEqual(true)

    wrapper = getWrapper({ flex: 3, justify: 'between', direction: 'row' })
    expect(wrapper.hasClass('Flex--flex-3')).toEqual(true)
    expect(wrapper.hasClass('Flex--justify-between')).toEqual(true)
    expect(wrapper.hasClass('Flex--direction-row')).toEqual(true)

    wrapper = getWrapper({ flex: 2, direction: 'column', align: 'center' })
    expect(wrapper.hasClass('Flex--flex-2')).toEqual(true)
    expect(wrapper.hasClass('Flex--direction-column')).toEqual(true)
    expect(wrapper.hasClass('Flex--align-center')).toEqual(true)
  })

  it('renders with additional class name if respective prop was passed', () => {
    const wrapper = getWrapper({
      className: 'Tirion'
    })
    expect(wrapper.hasClass('Flex Tirion')).toEqual(true)
  })
})
