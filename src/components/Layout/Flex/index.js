import React from 'react'

import './Flex.css'

function Flex (props) {
  const { children, className, align, direction, justify, flex, ...other } = props
  const getClassName = () => className ? ` ${className}` : ''
  const getAlign = () => align ? ` Flex--align-${align}` : ''
  const getDirection = () => direction ? ` Flex--direction-${direction}` : ''
  const getJustify = () => justify ? ` Flex--justify-${justify}` : ''
  const getFlex = () => flex ? ` Flex--flex-${flex}` : ''
  const combinedClassName = `Flex${getClassName()}${getAlign()}${getDirection()}${getJustify()}${getFlex()}`

  return (
    <div
      className={combinedClassName}
      {...other}
    >
      {children}
    </div>
  )
}

export default Flex
