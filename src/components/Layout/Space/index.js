import React from 'react'

import './Space.css'

function Space (props) {
  const { children, bottom, top, size, left, right, fullwidth } = props

  const getTop = () => top ? ' Space--top' : ''
  const getBottom = () => bottom ? ' Space--bottom' : ''
  const getLeft = () => left ? ' Space--left' : ''
  const getRight = () => right ? ' Space--right' : ''
  const getSize = () => size ? ` ${size}` : ''
  const getFullwidth = () => fullwidth ? ' Space--fullwidth' : ''
  const combinedClassName = `Space${getTop()}${getBottom()}${getLeft()}${getRight()}${getSize()}${getFullwidth()}`

  return (
    <div
      className={combinedClassName}
    >
      {children}
    </div>
  )
}

export default Space
