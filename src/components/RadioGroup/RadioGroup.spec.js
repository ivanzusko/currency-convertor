/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import RadioGroup from './index'

const mock = [{
  value: 'EUR'
}, {
  value: 'USD'
}, {
  value: 'GBP'
}]

describe('<Button> component', () => {
  const getWrapper = props => shallow(
    <RadioGroup
      options={mock}
      {...props}
    />
  )

  it('renders without crashing and with all options', () => {
    const wrapper = getWrapper()

    expect(wrapper.props().role).toEqual('radiogroup')
    expect(wrapper.find('.Radio')).toHaveLength(mock.length)
  })

  it('renders with option selected', () => {
    const wrapper = getWrapper({
      value: 'EUR'
    })

    expect(wrapper.find('span.isChecked').find('input').props().value).toEqual('EUR')
  })

  it('should handle onChange', () => {
    const onChange = jest.fn()
    const props = {
      value: 'EUR',
      onChange
    }
    const simulateChange = (input) => [
      input.props.onChange(input.props.value)
    ]

    const wrapper = getWrapper(props)
    const inputs = wrapper.find('input').getElements()

    simulateChange(inputs[0])
    expect(onChange).toBeCalledWith('EUR')

    simulateChange(inputs[1])
    expect(onChange).toBeCalledWith('USD')

    simulateChange(inputs[2])
    expect(onChange).toBeCalledWith('GBP')
  })
})
