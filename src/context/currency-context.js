import React, { createContext, useState, useContext } from 'react'

const CurrencyContext = createContext()

function CurrencyProvider (props) {
  const [currencyToExchange, setCurrencyToExchange] = useState('GBP')

  return (
    <CurrencyContext.Provider
      value={{
        currencyToExchange,
        setCurrencyToExchange
      }}
    >
      {props.children}
    </CurrencyContext.Provider>
  )
}

const useCurrency = () => useContext(CurrencyContext)

export { CurrencyProvider, useCurrency }
