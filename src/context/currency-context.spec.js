/* eslint-env jest */
import React from 'react'
import { act, renderHook } from '@testing-library/react-hooks'
import { CurrencyProvider, useCurrency } from './currency-context'

describe('Currency context', () => {
  it('useCurrency', () => {
    const wrapper = ({ children }) => (
      <CurrencyProvider>
        {children}
      </CurrencyProvider>
    )
    const { result } = renderHook(() => useCurrency(), { wrapper: wrapper })
    expect(result.current.currencyToExchange).toEqual('GBP')

    act(() => {
      result.current.setCurrencyToExchange('USD')
    })
    expect(result.current.currencyToExchange).toEqual('USD')

    act(() => {
      result.current.setCurrencyToExchange('EUR')
    })
    expect(result.current.currencyToExchange).toEqual('EUR')
  })
})
