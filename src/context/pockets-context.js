import React, { createContext, useState, useContext } from 'react'

const PocketsContext = createContext()
const MainPocketContext = createContext()

const pocketsObject = {
  EUR: {
    name: 'EUR',
    fullName: 'EUR - Euro',
    label: '€',
    amount: 100.13
  },
  USD: {
    name: 'USD',
    fullName: 'USD - American Dollar',
    label: '$',
    amount: 13
  },
  GBP: {
    name: 'GBP',
    fullName: 'GBP - British Pound',
    label: '£',
    amount: 777
  }
}

function PocketsProvider (props) {
  const [pockets, setPockets] = useState(pocketsObject)
  const [mainPocket, setMainPocket] = useState('EUR')

  return (
    <PocketsContext.Provider
      value={{
        pockets,
        setPockets
      }}
    >
      <MainPocketContext.Provider
        value={{
          mainPocket,
          setMainPocket
        }}
      >
        {props.children}
      </MainPocketContext.Provider>
    </PocketsContext.Provider>
  )
}

const usePockets = () => useContext(PocketsContext)
const useMainPocket = () => useContext(MainPocketContext)

export { PocketsProvider, usePockets, useMainPocket, pocketsObject }
