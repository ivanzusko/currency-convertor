/* eslint-env jest */
import React from 'react'
import { act, renderHook } from '@testing-library/react-hooks'
import { shallow } from 'enzyme'
import {
  PocketsProvider,
  pocketsObject,
  usePockets,
  useMainPocket
} from './pockets-context'

describe('context', () => {
  it('useMainPocket', () => {
    const { result } = renderHook(() => useMainPocket(), { wrapper: PocketsProvider })
    expect(result.current.mainPocket).toEqual('EUR')

    act(() => {
      result.current.setMainPocket('USD')
    })
    expect(result.current.mainPocket).toEqual('USD')

    act(() => {
      result.current.setMainPocket('GBP')
    })
    expect(result.current.mainPocket).toEqual('GBP')
  })

  it('usePockets', () => {
    const { result } = renderHook(() => usePockets(), { wrapper: PocketsProvider })
    expect(result.current.pockets).toEqual(pocketsObject)

    const updatedPockets = {
      name: 'Jon Snow',
      whoIs: 'bastard'
    }
    act(() => {
      result.current.setPockets(updatedPockets)
    })
    expect(result.current.pockets).toEqual(updatedPockets)
  })

  it('should render children', () => {
    const provider = shallow(
      <PocketsProvider>
        <div className='true-trolles'>Gate of Trolls</div>
      </PocketsProvider>
    )

    expect(provider.find('.true-trolles')).toHaveLength(1)
  })
})
