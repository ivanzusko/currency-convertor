import React, { createContext, useEffect, useState, useContext } from 'react'
import { useMainPocket } from './pockets-context'
import { useCurrency } from './currency-context'
import calculateRate from '../utils/calculate-rate'

const RatesContext = createContext()

function RatesProvider (props) {
  const { mainPocket } = useMainPocket()
  const { currencyToExchange } = useCurrency()
  const [rates, setRates] = useState(null)
  const [shouldFetchRates, setShouldFetchRates] = useState(true)
  const [rate, setRate] = useState(null)

  useEffect(() => {
    if (shouldFetchRates) {
      getRates()
        .then(data => {
          setRates(data.rates)
          setShouldFetchRates(false)

          // fetch fresh rates every 10 sec
          setTimeout(() => {
            setShouldFetchRates(true)
          }, 10000)
        })
    }
  }, [shouldFetchRates])

  useEffect(() => {
    if (rates) {
      // get rate
      const rate = calculateRate(rates, mainPocket, currencyToExchange)

      setRate(rate)
    }
  }, [rates, mainPocket, currencyToExchange])

  return (
    <RatesContext.Provider
      value={{
        rate,
        rates
      }}
    >
      {props.children}
    </RatesContext.Provider>
  )
}

async function getRates () {
  try {
    const result = await window.fetch(`${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_KEY}`)
    const responseData = await result.json()

    return responseData
  } catch (error) {
    return error
  }
}

const useRates = () => useContext(RatesContext)

export { RatesProvider, useRates, getRates }
