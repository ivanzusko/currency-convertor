/* eslint-env jest */
import React from 'react'
import { renderHook } from '@testing-library/react-hooks'
import { PocketsProvider } from '../context/pockets-context'
import { RatesProvider, useRates, getRates } from './rates-context'
import { CurrencyProvider } from './currency-context'

const mockRate = {
  AED: 3.67295,
  GBP: 0.9827,
  EUR: 76.850004,
  USD: 1
}
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(mockRate)
  })
)

describe('Currency context', () => {
  beforeEach(() => {
    global.fetch.mockClear()
  })

  it('returns rates', async () => {
    const response = await getRates()

    expect(global.fetch).toHaveBeenCalledTimes(1)
    expect(global.fetch).toHaveBeenCalledWith(
      `${process.env.REACT_APP_API_URL}${process.env.REACT_APP_API_KEY}`
    )
    expect(response).toEqual(mockRate)
  })

  it('returns exception', async () => {
    global.fetch.mockImplementationOnce(() =>
      Promise.reject(Error('You know nothing, Jon Snow'))
    )

    const response = await getRates()

    expect(response).toEqual(Error('You know nothing, Jon Snow'))
  })

  it('should render with initial state', async () => {
    const wrapper = ({ children }) => (
      <PocketsProvider>
        <CurrencyProvider>
          <RatesProvider>
            {children}
          </RatesProvider>
        </CurrencyProvider>
      </PocketsProvider>
    )

    const { result } = renderHook(useRates, { wrapper })
    expect(result.current.rate).toEqual(null)
    expect(result.current.rates).toEqual(null)
  })
})
