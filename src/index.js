import React from 'react'
import ReactDOM from 'react-dom'
import { CurrencyProvider } from './context/currency-context'
import { RatesProvider } from './context/rates-context'
import { PocketsProvider } from './context/pockets-context'
import './index.css'
import App from './components/App'

ReactDOM.render(
  <React.StrictMode>
    <PocketsProvider>
      <CurrencyProvider>
        <RatesProvider>
          <App />
        </RatesProvider>
      </CurrencyProvider>
    </PocketsProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
