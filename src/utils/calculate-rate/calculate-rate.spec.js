import calculateRate from './index'

describe('calculateRate util', () => {
  const mockRates = {
    ETB: 37.263562,
    EUR: 0.848076,
    FJD: 2.1142,
    GBP: 0.766057,
    GEL: 3.205,
    UAH: 28.266432,
    UGX: 3702.09978,
    USD: 1,
    Targaryen: 1,
    Lannister: 3
  }

  it('should calculate the actual currency rate', () => {
    expect(calculateRate(mockRates, 'EUR', 'USD')).toEqual(1.1791396054127223)
    expect(calculateRate(mockRates, 'USD', 'EUR')).toEqual(0.848076)
    expect(calculateRate(mockRates, 'Targaryen', 'Lannister')).toEqual(3)
    expect(calculateRate(mockRates, 'Lannister', 'Targaryen')).toEqual(0.3333333333333333)
  })
})
