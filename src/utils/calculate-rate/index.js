/**
 * @description function which calculates the actual currency rate
 * @param {Object} rates rates from api response
 * @param {String} currencyFrom the currency which has to be exchanged
 * @param {String} currencyTo the currency which will be converted to
 *
 * @returns {Number} actual exchange rate
 */
function calculateRate (rates, currencyFrom, currencyTo) {
  return rates[currencyTo] / rates[currencyFrom]
}

export default calculateRate
