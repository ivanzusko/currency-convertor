import dynamicToDecimal from './index'

describe('dynamicToDecimal util', () => {
  it('should convert number(string number) to a number with specified amount of chars after coma', () => {
    expect(dynamicToDecimal('13.1254', 2)).toEqual(13.13)
    expect(dynamicToDecimal('13.1234', 2)).toEqual(13.12)
    expect(dynamicToDecimal('13.1221', 2)).toEqual(13.12)
    expect(dynamicToDecimal('13.12', 2)).toEqual(13.12)
    expect(dynamicToDecimal('13.1', 2)).toEqual(13.1)
    expect(dynamicToDecimal('13', 2)).toEqual(13)
  })
})
