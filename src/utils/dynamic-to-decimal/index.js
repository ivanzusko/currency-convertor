import toDecimal from '../to-decimal'

/**
 * @description this function converts number(string number) to a number
 *              with specified amount of chars after comma. Should be used inside
 *              input change handlers
 * @param {Number|String} value Numer or string representing a number
 * @param {Number} decimal How many decimals should be returned within the number
 *
 * @returns {Number} Number with required amount of decimalss
 */
function dynamicToDecimal (value, decimal) {
  const valueString = String(value)
  const [number, decimals] = valueString.split('.')

  // if there are more decimals then we need
  if (decimals && decimals.length > decimal) {
    // we will take required amount of `decimals` + 1 more
    // so later we will pass the new value to `toDecimal` method
    // and will have properly rounded value
    // e.g. if we needd 2 decimals we will pass 3 in order to get correct 2:
    // 13.123 => 13.12, 13.125 => 13.13
    const newValue = number + '.' + decimals.slice(0, decimal + 1)

    return toDecimal(newValue, decimal)
  }

  return value * 1
}

export default dynamicToDecimal
