import toDecimal from '../to-decimal'
/**
 * @description this function takes input's value and modifies it
 * @param {String} initialValue Value which is in the state before change *
 * @param {String} value new value from input
 *
 * @returns {String} empty string, initialValue, passed value(negatove/0/positive number), or `0.`
 */
function formatValue (initialValue, value) {
  const maxDecimals = 2
  const decimalSeparator = '.'

  if (value === '0') {
    return '0'
  }

  if (!value) {
    return ''
  }

  // cover case when user paste/type huge number
  if (Number(value) > (Math.pow(2, 53) - 1)) {
    return String(Math.pow(2, 53) - 1)
  }

  // allow to enter serapator first
  if (value === decimalSeparator || value === ',') {
    return `0${decimalSeparator}`
  }

  if (value.includes(decimalSeparator) && value.split(decimalSeparator)[1].length > maxDecimals) {
    const newValue = toDecimal(value, maxDecimals)

    // cover special case when url gets into input (eg drag-n-drop `cancel` button)
    // it has `decimalSerarator` and it will match second condition, so
    // `newValue` in this case would be NaN
    if (isNaN(newValue)) {
      return initialValue
    }

    return String(newValue)
  }

  const modifiedValue = value.replace(',', decimalSeparator).replace(' ', '')

  if (isNaN(modifiedValue)) {
    return initialValue
  }

  // make sure that 0 can be the only digit before comma and if it follows by int - 0 should be omited
  // e.g.: 0.12 => 0.12, 01.13 => 1.13, 007.69 => 7.69
  if (modifiedValue.includes(decimalSeparator)) {
    const [head, tail] = modifiedValue.split(decimalSeparator)

    return `${Number(head)}${decimalSeparator}${tail}`
  } else {
    // take care input like 00, 01, 09 => 0.0, 0.1, 0.9
    if (modifiedValue.startsWith('0') && modifiedValue.length === 2) {
      const tail = modifiedValue.slice(1)

      return `0${decimalSeparator}${tail}`
    }

    // take care about inserted numbers like 001332, +02312, -001231
    if (
      (modifiedValue.startsWith('0') || modifiedValue.startsWith('+0') || modifiedValue.startsWith('-0'))
    ) {
      return String(Number(modifiedValue))
    }
  }

  return modifiedValue
}

export default formatValue
