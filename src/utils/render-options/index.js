/**
 * @description this function maps the pockets to become an array of options
 * @param {Object} pockets Object with all pockets
 *
 * @returns {Array<Object>} Array of options [{ value: String }]
 */
function renderOptions (pockets) {
  return Object.keys(pockets).map(item => ({
    value: item
  }))
}

export default renderOptions
