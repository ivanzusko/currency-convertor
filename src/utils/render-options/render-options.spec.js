import renderOptions from './index'

describe('renderOptions utils', () => {
  const mockPockets = {
    EUR: {
      name: 'EUR',
      fullName: 'EUR - Euro',
      label: '€',
      amount: 100.13
    },
    USD: {
      name: 'USD',
      fullName: 'USD - American Dollar',
      label: '$',
      amount: 13
    },
    GBP: {
      name: 'GBP',
      fullName: 'GBP -  British Pound',
      label: '£',
      amount: 777
    }
  }

  it('should map the pockets to become an array of options', () => {
    expect(renderOptions(mockPockets)).toEqual([
      {
        value: 'EUR'
      }, {
        value: 'USD'
      }, {
        value: 'GBP'
      }
    ])
  })
})
