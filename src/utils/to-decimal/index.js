/**
 * @description this function converts number(string number) to a number
 *              with specified amount of chars after comma
 * @param {Number|String} value
 * @param {Number} length
 *
 * @returns {Number} Number with two chars after coma
 */
function toDecimal (value, length) {
  return parseFloat(value).toFixed(length) * 1
}

export default toDecimal
