import toDecimal from './index'

describe('toDecimal util', () => {
  it('should convert number(string number) to a number with specified amount of chars after coma', () => {
    expect(toDecimal('13.1221', 2)).toEqual(13.12)
    expect(toDecimal('13.12', 2)).toEqual(13.12)
    expect(toDecimal('13.1', 2)).toEqual(13.1)
    expect(toDecimal('13', 2)).toEqual(13)
  })
})
